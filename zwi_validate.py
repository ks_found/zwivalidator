#!/usr/bin/env python3.9
import zipfile
import argparse
from pathlib import Path
import json
from hashlib import sha1
import logging
import sys

logger = logging.getLogger('zwi_validate')

class ZWIInvalidFilename(Exception):
    pass

class ZWIMissing(Exception):
    pass

class ZWIUnexpected(Exception):
    pass

class ZWIBadChecksum(Exception):
    pass

class ZWIValueError(Exception):
    pass


class ValidationMethods:
    @classmethod
    def validate_ZWIversion(v):
        # Raises ValueError if invalid number
        float(v)

    @classmethod
    def validate_Title(v):
        if not v:
            raise ZWIValueError(f'Empty Title')

    @classmethod
    def validate_Lang(v):
        if not len(v) == 2 or not v.isalpha():
            raise ZWIValueError(f'Invalid Lang "{v}"')

    @classmethod
    def validate_Content(v):
        # List of article files
        pass

    @classmethod
    def validate_Primary(v):
        # Path should exist in ZWI
        pass

    @classmethod
    def validate_Revisions(v):
        # Unused
        pass

    @classmethod
    def validate_Publisher(v):
        # Person or organization who published the article
        if not v:
            raise ZWIValueError("Empty Publisher")

    @classmethod
    def validate_CreatorNames(v):
        # A list of people who created the article. Can be real names or usernames.
        for name in v:
            if type(name) != str:
                raise ZWIValueError(f"Bad CreatorNames value {name}")

    @classmethod
    def validate_ContributorNames(v):
        # A list of people who worked on the article
        for name in v:
            if type(name) != str:
                raise ZWIValueError(f"Bad ContributorNames value {name}")

    @classmethod
    def validate_LastModified(v):
        # The last time the article was modified, in Unix epoch (seconds) format
        int(v)

    @classmethod
    def validate_TimeCreated(v):
        # The time the article was created, in Unix epoch format
        int(v)

    @classmethod
    def validate_Categories(v):
        pass

    @classmethod
    def validate_Topics(v):
        pass

    @classmethod
    def validate_Rating(v):
        pass

    @classmethod
    def validate_Description(v):
        pass

    @classmethod
    def validate_ShortDescription(v):
        pass

    @classmethod
    def validate_Comment(v):
        pass

    @classmethod
    def validate_License(v):
        pass

    @classmethod
    def validate_SourceURL(v):
        pass

def validate_metadata_json(root, args):
    j = json.load((root/'metadata.json').open())

    if j['ZWIversion'] == 1.3:
        required = set(("ZWIversion Title Lang Content Primary Revisions Publisher CreatorNames ContributorNames " \
                   "LastModified TimeCreated Categories Topics Rating Description ShortDescription Comment License " \
                   "SourceURL").split())
        if missing := required - set(j.keys()):
            raise ZWIMissing(f'Missing fields {missing}')

        for k, v in j.items():
            if method := getattr(ValidationMethods, f'validate_{k}', None):
                method(v)
            else:
                raise ZWIUnexpected(f'Unexpected metadata attribute {k}')
    else:
        raise ZWIUnexpected(f"Unsupported ZWIversion {j['ZWIversion']}")

def file_sha1(path):
    BLOCKSIZE = 65536
    hasher = sha1()
    with path.open('rb') as afile:
        buf = afile.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afile.read(BLOCKSIZE)
    return hasher.hexdigest()


def validate_data_dir(infolist, root, args):
    media = root/'media.json'

    datafiles = set(x.filename for x in infolist if x.filename.startswith('data/'))

    with media.open() as m:
        j = json.load(m)
        manifest = set(j.keys())
        missing_from_datafiles = manifest - datafiles
        missing_from_manifest = datafiles - manifest
        if missing_from_datafiles:
            raise ZWIMissing
        if missing_from_manifest:
            raise ZWIMissing

        if args.checksums:
            for filename, checksum in j.items():
                if file_sha1(root/filename) != checksum:
                    raise ZWIBadChecksum


path = "/home/user/encyclosphere/ZWI/en/wikipedia/en.wikipedia.org/wiki#History_of_Germany.zwi"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Validates a ZWI")
    parser.add_argument("file", help="Path to the .zwi file")
    parser.add_argument("-c", "--checksums", action='store_true', help="Verify checksums.")
    parser.add_argument("-v", "--verbose", action='store_true', help="Increase verbosity.")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(stream=sys.stdout, level=logging.INFO)

    logger.info("Checking %s", args.file)

    zf = zipfile.ZipFile(args.file)
    infolist = zf.infolist()
    root = zipfile.Path(zf)

    validate_data_dir(infolist, root, args)
    validate_metadata_json(root, args)

    for p in root.iterdir():
        if p.is_file():
            n = Path(p.name)
            
            if n.stem == "article":
                if n.suffix == "" or n.suffix not in ('.html', '.md', '.wikitext', '.txt'):
                    raise ZWIInvalidFilename(f'Invalid article suffix "{n.suffix}"')
                continue
            
            if p.name == 'media.json':
                # Will validate json in validate_data_dir()
                continue
            
            if p.name == 'metadata.json':
                # Validated by validate_metadata_json()
                continue

            raise ZWIInvalidFilename(f'Unrecognized file {p}')

        elif p.is_dir():
            if p.name == "data":
                if not (root/'media.json').is_file():
                    raise ZWIMissing
                continue

            raise ZWIInvalidFilename(f'Unrecognized directory {p}')
        else:
            raise ZWIInvalidFilename(f'Unrecognized path {p}')

            










